from pprint import pprint

COMMAND = ":op"
ADMIN_LIST = ('User1', 'User2', 'User3')

class Demo(object):
    """This is demo plugin, that is showing how the plugins should have beed
    written.
    """
    def __init__(self):
        print "Give op plugin loaded."
        self.command = COMMAND
        self.admin_list = ADMIN_LIST

    def __call__(self, server):
        """What to do when running plugin."""
        # alias
        data = server.msg_data
        try:
            # if command is COMMAND
            # this may throw IndexError
            if not data['msg_list'][0] == self.command:
                return 
            # if user is logged
            if not data['prefix'].startswith(data['user'] + r'!i='):
                return
            # if user is on admin list
            if not data['user'] in self.admin_list:
                return
            # else, give op
            server.send_raw('MODE %s +o %s' % (data['target'], data['user']))
        except IndexError:
            pass
