from pprint import pprint


class Demo(object):
    """This is demo plugin, that is showing how the plugins should have beed
    written.
    """
    def __init__(self):
        print "Demo plugin loaded."

    def __call__(self, server):
        """What to do when running plugin."""
        print "-+-" * 20
        pprint(server.msg_data)
