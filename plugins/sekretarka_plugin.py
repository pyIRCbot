#!/usr/bin/python
# -*- coding: utf-8-*-

import cPickle
import os
import os.path
import time

COMMAND = ":msg"
USAGE_INFO = ":msg <osoba> <wiadomość>"
MESSAGE_FORMAT = ">> %s o %s prosil zebym przekazal : %s "
MESSAGES_SAVE_FILE = \
    os.path.join(os.path.dirname(__file__), "sekretarka.pickle")


class Sekretarka(object):
    def __init__(self):
        self.messages = {}
        # load pickle if exist
        if os.path.isfile(MESSAGES_SAVE_FILE):
            cp_file = open(MESSAGES_SAVE_FILE, 'r')
            self.messages = cPickle.loads(cp_file.read())
            cp_file.close()
            os.remove(MESSAGES_SAVE_FILE)

    def __del__(self):
        cp_file = open(MESSAGES_SAVE_FILE, 'w')
        cPickle.dump(self.messages, cp_file)
        cp_file.close()
        
    def __call__(self, server):
        if not server.msg_data['command'] == 'pubmsg':
            return 
        self.save_msg(server)
        self.return_msg(server)

    def return_msg(self, server):
        """Return message if `nick` just connected od said something."""
        if server.msg_data['command'] == 'join' or \
                server.msg_data['command'] == 'pubmsg':
            if server.msg_data['user'] in self.messages:
                # send messages to `nick`
                n = server.msg_data['user']
                for ddict in self.messages[n]:
                    # send each message from self.messages list
                    server.privmsg(n, MESSAGE_FORMAT % \
                            (ddict['from'], ddict['date'], ddict['text']))
                # remove messages from dict
                del self.messages[n]

    def save_msg(self, server):
        """If COMMAND, save message."""
        msg = server.msg_data['msg_list']
        try:
            if msg[0] == COMMAND:
                # if message is `:cmd  <nick>`
                if len(msg) < 3:
                    server.privmsg(server.msg_data['user'], USAGE_INFO )
                # if message is `:cmd <nick> <something> [<something more>]`
                else:
                    nick = msg[1]
                    text = " ".join(msg[2:])
                    if not nick in self.messages:
                        self.messages[nick] = []
                    # message structure
                    info_data = {
                            "from"  : server.msg_data['user'],
                            "date"  : time.strftime("%H:%M"),
                            "text"  : text,
                            }
                    self.messages[nick].append(info_data)
        except IndexError:
            pass



