import sqlalchemy as sql
from time import strftime

# for more info see
# http://www.sqlalchemy.org/docs/04/intro.html#overview_sqlalchemy
DATABASE_PATH = "sqlite://../../django/irclog/django_sqlite3_database.db"

class _DatabaseConnection(object):
    """Connect to database. 
    Class name starts with `_` becouse it's not a plugin - it's privete class
    and it shouldn't be loaded
    """
    def __init__(self, dblink):
        """Create database connection"""
        self.database = sql.create_engine(dblink)
        metadata =  sql.MetaData(self.database)
        self.message = sql.Table('log_message', metadata, autoload=True)
        self.user = sql.Table('log_user', metadata, autoload=True)

    def add_msg(self, user_id, text):
        """Add new message. Doesn't check if user_id exist"""
        # example 2008-02-17 18:03:46.072746
        # TODO 
        now = strftime("%Y-%m-%d %H:%M:%S.000000")
        text = unicode(text, encoding="utf-8")
        i = self.message.insert()
        i.execute(user_id_id=user_id, date=now, text=text)

    def add_user(self, nick):
        """Add new user. Doesn't check if allready exist
        Return his new id.
        """
        i = self.user.insert()
        i.execute(nick_name=nick, first_name='', last_name='',
                  about='', homepage='', email='', avatar='')
        return self.get_user(nick)

    def get_user(self, nick):
        """Find user and returns his id, or None if doesn't exist"""
        s = self.user.select(self.user.c.nick_name == nick)
        try:
            return s.execute().fetchone()[0]
        except TypeError:
            return None

class Log(object):
    def __init__(self):
        self.db = _DatabaseConnection(DATABASE_PATH)
        self.users = {}

    def __call__(self, server):
        if server.msg_data['command'] in ('join', 'quit', 'part'):
            msg = " *******   " + server.msg_data['command'] 
        elif server.msg_data['command'] == 'pubmsg':
            msg = server.msg_data['args'][0]
        else:
            # do nothing, just end
            return
        nick = server.msg_data['user']
        if not nick in self.users.keys():
            self.users[nick] = self.db.get_user(nick)
            if not self.users[nick]:
                self.users[nick] = self.db.add_user(nick)
        # /me message
        if msg[1:].startswith('ACTION'):
            msg = "**" + msg[7:-1]
        # add message to database
        self.db.add_msg(self.users[nick], msg)


