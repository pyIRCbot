# -*- coding: utf-8 -*-
import time

COMMAND = ':seen'
MESSAGE_FORMAT = '%s: %s, ostatnio coś mówił o %s'
USAGE = 'chcesz wiedziac jak uzywac? nie powiem!'

IS_HERE = 'jest tutaj'
NOT_HERE = 'nie ma go tutaj'

SEEN_SEEN = '%s: hyh, nie, nie nie!'
SEEN_YOU = '%s: zabawny jestes...'
SEEN_NOT = '%s: nie widzialem, przepraszam! ;('

class LastSeen(object):

    def __init__(self):
        self.lseen = {}

    def __call__(self, server):
        try:
            if server.msg_data['command'] in ['join', 'pubmsg', 'privmsg']:
                self.lseen[server.msg_data['user']] = [time.strftime('%H:%M'),
                                                       IS_HERE ]
            elif server.msg_data['command'] in ['kick', 'part', 'quit']:
                self.lseen[server.msg_data['user']] = [time.strftime('%H:%M'),
                                                       NOT_HERE ]
            elif server.msg_data['command'] == 'nick':
                self.lseen[server.msg_data['target']] = [time.strftime('%H:%M'),
                                                       IS_HERE]
            self.get_info(server)
        except IndexError:
            pass
        
    
    def get_info(self, server):
        if server.msg_data['msg_list'][0] == COMMAND \
            and len(server.msg_data['msg_list']) == 2:
            if server.msg_data['msg_list'][1] == COMMAND:
                server.privmsg(server.msg_data['target'],
                               SEEN_SEEN % server.msg_data['user'])
            elif server.msg_data['msg_list'][1] == server.msg_data['user']:
                server.privmsg(server.msg_data['target'],
                               SEEN_YOU % server.msg_data['user'])
            else:
                if not self.lseen.has_key(server.msg_data['msg_list'][1]):
                    server.privmsg(server.msg_data['target'],
                                   SEEN_NOT % server.msg_data['user'])
                else:
                    user = self.lseen[server.msg_data['msg_list'][1]]
                    server.privmsg(server.msg_data['target'],
                                   MESSAGE_FORMAT % (server.msg_data['user'],
                                                    user[1], user[0]))
        elif server.msg_data['msg_list'][0] == COMMAND \
             and len(server.msg_data['msg_list']) == 1:
            server.privmsg(server.msg_data['target'], USAGE)
                        
                        
