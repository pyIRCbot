import os.path

# where the plugins are..
PLUGIN_PATH = os.path.join(os.path.dirname(__file__), "plugins")
LIBS_PATH = os.path.join(os.path.dirname(__file__), "libs")

DEBUG = True
RELOAD_PLUGINS =  ":reloadplug"

CONNECT_CHANNEL = "#testme"
CONNECT_HOST = "irc.freenode.net"

IRC_NAME = "pyIRCbot"
IRC_IDENT = "pyIRCbot_test"
IRC_REALNAME = "John Smith"
IRC_PASSWORD = "secretpassword"
