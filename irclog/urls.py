from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # admin
    (r'^admin/', include('django.contrib.admin.urls')),
    
    (r'^$',                             'irclog.log.views.index'),
    (r'^old/(\d{1,3})/$',               'irclog.log.views.index'),
    (r'^user/([^/]+)/$',                'irclog.log.views.user'),
    (r'^search/$',                      'irclog.log.views.search'),
)
