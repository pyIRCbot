from django.db import models


class User(models.Model):
    nick_name = models.CharField(maxlength=20)
    first_name = models.CharField(maxlength=14, blank=True)
    last_name = models.CharField(maxlength=25, blank=True)
    about = models.TextField(blank=True)
    homepage = models.URLField(blank=True)
    email = models.EmailField(maxlength=50, blank=True)
    avatar = models.ImageField(height_field=50, width_field=50, \
             upload_to="avatars", blank=True)

    def __str__(self):
        return self.nick_name

    class Admin():
        pass


class Message(models.Model):
    user_id = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    
    class Meta:
        ordering = ["-date"]
    
    def __str__(self):
        return self.text

    class Admin():
        pass


