from django.shortcuts import render_to_response
from irclog.log.models import Message, User
from django.db.models import Q

def index(request, from_msg=0):
    from_msg = int(from_msg)
    to_msg = from_msg + 20
    prev_msg = from_msg - 20
    msg_list = Message.objects.all()[from_msg:to_msg]
    return render_to_response("log/index.html", locals())

def user(request, nick):
    user = User.objects.get(nick_name=nick)
    user_msg = user.message_set.all()[:20]
    return render_to_response("log/user.html", locals())

def search(request):
    query = request.GET.get('q', '')
    if query:
        qset = (
                Q(text__icontains=query)
                )
        results = Message.objects.filter(qset)[:20]
    else:
        results = []
    return render_to_response("log/search.html", 
            {
                "results": results,
                "query": query
            })
