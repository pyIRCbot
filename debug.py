import configuration as conf


class Debug(object):
    def __init__(self):
        self.debug = conf.DEBUG
        if self.debug:
            print ">> debug mode is on."
    

    def debug(self):
        """Method called by PyIRCBot. 

        To add debugger test, just call it here.
        """
        if self.debug:
            self.reload_plugins()

    def reload_plugins(self):
        """Reload plugin list"""
        try:
            if self.server.msg_data['msg_list'][0] == conf.RELOAD_PLUGINS:
                self.plugins = []
                self.plugins_load()
                print ">> Plugins reloaded.."
        except IndexError:
            pass
