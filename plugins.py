import sys
import os
import os.path


class Plugins(object):
    def __init__(self, plugins_path, suffix=".py"):
        self.plugins_path = plugins_path
        self.suffix = suffix
        self.plugins = []

    def plugin_remove(self, plugname):
        """Unload plugin."""
        if not self.plugins:
            return
        for number, plugin in enumerate(self.plugins):
            if plugin.__name__ == plugname:
                del self.plugins[number]

    def plugin_load(self, plugname):
        pass

    def plugins_reload(self, *args):
        """Unload plugins and then load them again"""
        del self.plugins
        self.plugins_load(args)

    def plugins_load(self, *args):
        """import all modules from plugins_path, that ends with suffix.
        Create instance of each class from loaded modules with args as
        constructor arguments.
        """
        pluglist = []
        # add plugins_path to sys.path, that I could import from there
        if self.plugins_path not in sys.path:
            sys.path.append(self.plugins_path)
        # import all plugins from `plugins_path`
        for m_name in os.listdir(self.plugins_path):
            m_path = os.path.abspath(os.path.join(self.plugins_path, m_name))
            m_name, m_extension = os.path.splitext(m_name)
            # if file shouldn't be load as plugin
            if os.path.islink(m_path) or \
                    m_path == __file__ or \
                    not m_extension == self.suffix:
                        continue
            # if module is allready loaded, unload it
            for key in sys.modules.keys():
                if key == m_name:
                    del sys.modules[key]
            module = __import__(m_name)
            pluglist.append(module)
        # load every class from module
        for module in pluglist:
            for obj in dir(module):
                # check if it's class
                # TODO  2008-02-24 12:18:01 
                # better class checking
                if not obj.startswith("_"):
                    try:
                        # make temp instance and check it's class
                        temp_ins = getattr(module, obj)(*args)
                        self.plugins.append(temp_ins)
                    except TypeError:
                        # should I show some info?
                        pass

