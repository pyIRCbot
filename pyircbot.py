import os
import sys

from plugins import Plugins
import configuration as conf
from debug import Debug

# libs 
if conf.LIBS_PATH not in sys.path:
    sys.path.append(conf.LIBS_PATH)
import irclib


class PyIRCBot(Plugins, Debug):
    def __init__(self):
        Plugins.__init__(self, conf.PLUGIN_PATH)

    def connect(self):
        """Connect to host. Set name. Identify"""
        self.irc = irclib.IRC()
        self.server = self.irc.server()
        self.server.connect(conf.CONNECT_HOST, 6667, conf.IRC_NAME, 
                conf.IRC_PASSWORD, conf.IRC_IDENT, conf.IRC_REALNAME)
        self.server.join(conf.CONNECT_CHANNEL)
        
    def run_plugins(self):
        """Run each plugin"""
        # get the message and save it.
        self.server.msg_data = self.server.process_data()
        # save msg dict as msg_data['msg_list'] 
        # no more message split needed ;)
        try:
            self.server.msg_data['msg_list'] = \
                self.server.msg_data['args'][0].split()
        except (IndexError, TypeError):
            self.server.msg_data['msg_list'] = ''
        for plugin in self.plugins:
            plugin(self.server)

    def loop(self):
        """Start the main loop."""
        while self.server.connected:
            self.run_plugins()
            self.debug()


if __name__ == "__main__":
    bot = PyIRCBot()
    try:
        # load plugins
        bot.plugins_load()
        # connect to server
        bot.connect()
        # start the loop
        bot.loop()
    except KeyboardInterrupt:
        print "Exit."
